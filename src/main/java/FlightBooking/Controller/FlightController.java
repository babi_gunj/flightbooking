package FlightBooking.Controller;

import FlightBooking.Model.AjaxResponseBody;
import FlightBooking.Model.Flight;
import FlightBooking.Model.SearchCriteria;
import FlightBooking.Repository.FlightRepository;
import FlightBooking.Service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FlightController {

   // FlightRepository flightRepository = new FlightRepository();
    //SearchService searchService = new SearchService(flightRepository);

    FlightRepository flightRepository;
    SearchService searchService;
    List<Flight> flightSearched;
    @Autowired
    public void setFlightRepository(FlightRepository flightRepository){
        this.flightRepository = flightRepository;
    }
    @Autowired
    public void setSearchService(SearchService searchService){
        this.searchService = searchService;
    }
    @PostMapping(value="/api/search")
    public ResponseEntity<?> getSearchResultViaAjax(@Valid @RequestBody SearchCriteria search, Errors errors) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat();
        String newDate;
        Date date = search.getDatepicker();


        AjaxResponseBody result = new AjaxResponseBody();


        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {

          // result.setMsg(errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(",")));
            return ResponseEntity.badRequest().body(result);
        }
        if(date==null){

            Calendar c = Calendar.getInstance();
            try{
                c.setTime(sdf.parse(sdf.format(new Date())));
            }catch(ParseException e){
                e.printStackTrace();
            }
            //Incrementing the date by 1 day
            c.add(Calendar.DAY_OF_MONTH, 1);
            newDate= sdf.format(c.getTime());
            date = sdf.parse(newDate);

        }

       // flightRepository.getAll();
        flightSearched = searchService.search(search.getSource(),search.getDestination(),Integer.parseInt(search.getTravellers()),date,search.getFlightType());
        /*if (flightSearched.isEmpty()) {
            result.setMsg("no flights found!");
        } else {
            result.setMsg("success");
        }*/
        result.setResult(flightSearched);
        //String html = MakeHTML.makeHTML(flights);

        return ResponseEntity.ok(result);
    }
}
