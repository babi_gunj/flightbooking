package FlightBooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlightBookingSpringApplication {
    public static void main(String args[]) {
        SpringApplication.run(FlightBookingSpringApplication.class, args);
    }
}
