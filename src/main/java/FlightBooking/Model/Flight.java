package FlightBooking.Model;

import java.util.Date;

public class Flight {
    int id;
    String name;
    String source;
    String destination;
    int noOfPassengers;
    int noOfSeatsLeft;
    int totalNoOfSeats;
    Date departureDate;
    String type;
    double fare;

    public Flight(int i, String hyderabad, String latvia, String boeing77, int i1, int i2, int i3, Date parse, String business) {
    }

    public double getFare() {
        return fare;
    }

    public void setFare(double fare) {
        this.fare = fare;
    }



    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNoOfPassengers() {
        return noOfPassengers;
    }

    public void setNoOfPassengers(int noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public int getNoOfSeatsLeft() {
        return noOfSeatsLeft;
    }

    public void setNoOfSeatsLeft(int noOfSeatsLeft) {
        this.noOfSeatsLeft = noOfSeatsLeft;
    }

    public int getTotalNoOfSeats() {
        return totalNoOfSeats;
    }

    public void setTotalNoOfSeats(int totalNoOfSeats) {
        this.totalNoOfSeats = totalNoOfSeats;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Flight(int id, String source, String destination, String name, int noOfPassengers,
                  int noOfSeatsLeft, int totalNoOfSeats, Date departureDate,String type,int fare){
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.name = name;
        this.noOfPassengers = noOfPassengers;
        this.noOfSeatsLeft = noOfSeatsLeft;
        this.totalNoOfSeats = totalNoOfSeats;
        this.departureDate = departureDate;
        this.type = type;
        this.fare = fare;
    }
}
