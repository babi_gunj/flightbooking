package FlightBooking.Model;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import java.util.Date;


public class SearchCriteria {
    //@NotBlank(message = "Source Can't be Empty!")
    String Source;
    String Destination;
    String Travellers;
    Date datepicker;
    String flightType;

    public String getFlightType() {
        return flightType;
    }

    public void setFlightType(String flightType) {
        this.flightType = flightType;
    }

    public Date getDatepicker() {
        return datepicker;
    }

    public void setDatepicker(Date datepicker) {
        this.datepicker = datepicker;
    }

    public String getTravellers() {
        return Travellers;
    }

    public void setTravellers(String travellers) {
        this.Travellers = travellers;
    }





    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        this.Source = source;
    }

    public String getDestination() {
        return Destination;
    }

    public void setDestination(String destination) {
        this.Destination = destination;
    }

}
