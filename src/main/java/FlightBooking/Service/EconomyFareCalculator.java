package FlightBooking.Service;

import FlightBooking.Model.Flight;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EconomyFareCalculator implements FareCalculator {
    @Override
    public List<Flight> calculateFare(List<Flight> filteredFlights, int noOfPassengers, Date travelDate) {
        double fare=0;
        float percentage;
        List<Flight> flights = new ArrayList<>();
        for(Flight flight:filteredFlights) {
            // System.out.println(((float)(flight.getTotalNoOfSeats() - flight.getNoOfSeatsLeft() + noOfPassengers) / (flight.getTotalNoOfSeats()))*100);
            percentage = ((float)(flight.getTotalNoOfSeats() - flight.getNoOfSeatsLeft() + noOfPassengers) / (flight.getTotalNoOfSeats()))*100 ;
            if (percentage >= 0 && percentage < 40)
                fare = flight.getFare();
            else if (percentage >= 40 && percentage < 90)
                fare = flight.getFare() + (0.30 * flight.getFare());
            else if (percentage >= 90 && percentage <= 100)
                fare = flight.getFare() + (0.60 * flight.getFare());

            flight.setFare(fare*noOfPassengers);
            flights.add(flight);
        }
        return flights;
    }
}
