package FlightBooking.Service;

import FlightBooking.Model.Flight;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BusinessFareCalculator implements FareCalculator {
    @Override
    public List<Flight> calculateFare(List<Flight> filteredFlights, int noOfPassengers, Date travelDate) {
        double fare =0;
        float percentage;
        String day;
        List<Flight> flights = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");

        for(Flight flight:filteredFlights){
            day = sdf.format(flight.getDepartureDate());
            // System.out.println(day);
            if(day.equals("Monday") || day.equals("Friday") || day.equals("Sunday"))
                fare = ((0.4 * flight.getFare()) + flight.getFare() ) * noOfPassengers;
            else
                fare = flight.getFare() * noOfPassengers;
            flight.setFare(fare);
            flights.add(flight);
        }
        return flights;
    }
}
