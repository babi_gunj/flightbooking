package FlightBooking.Service;

import FlightBooking.Model.Flight;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FirstFareCalculator implements FareCalculator{

    @Override
    public List<Flight> calculateFare(List<Flight> filteredFlights, int noOfPassengers, Date travelDate) {
        List<Flight> flights = new ArrayList<>();
        double fare;
        float diff;
        for(Flight flight:filteredFlights){
            // System.out.println( Math.abs((flight.getDepartureDate().getTime() - travelDate.getTime())/86400000));
            diff = Math.abs((flight.getDepartureDate().getTime() - travelDate.getTime())/86400000);
            diff = 10-diff;
            fare =((flight.getFare() + (flight.getFare() * ((diff / 10))) )* noOfPassengers);
            flight.setFare(fare);
        }
        return filteredFlights;
    }
}
