package FlightBooking.Service;

import FlightBooking.Model.Flight;

import java.util.Date;
import java.util.List;

public interface FareCalculator {
     List<Flight> calculateFare(List<Flight> flight, int noOfPassengers, Date travelDate);
}
