package FlightBooking.Service;

import FlightBooking.Model.Flight;
import FlightBooking.Repository.FlightRepository;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
@Service
public class SearchService
{

    private FlightRepository flightRepository;


    public SearchService(FlightRepository flightRepository) throws ParseException {
        this.flightRepository = flightRepository;

    }

    static List<Flight> filteredFlights;
    static List<Flight> flight;




    public List<Flight> search(String source, String destination, int noOfPassengersToTravel, Date datepicker,String type) throws ParseException {
        final int noOfPassengers;

        List<Flight> flights = flightRepository.allFlights();
        FareCalculator flightsAfterFareCalculation;

        // List<Flight>  flights = flightRepository.allFlights();
        if(flights.size() == 0) {
            return new ArrayList<>();
        } else {
            if(noOfPassengersToTravel!=0) noOfPassengers = noOfPassengersToTravel;
            else noOfPassengers = 1;

            //System.out.println(datepicker);
        //  System.out.println(flights.stream().map(i->i.getType()).collect(Collectors.toList()));
          //  System.out.println(flights.stream().map(i->i.getDepartureDate()).collect(Collectors.toList()));
            /*for(Flight flight:flights)
                System.out.println(flight.getDepartureDate() + flight.getType() + flight.getDestination() + flight.getName() + flight.getId());
            for(Flight flight:flights){
                if(flight.getSource().equals(source)&& flight.getDestination().equals(destination)){
                    if(flight.getTotalNoOfSeats()>noOfPassengers && (datepicker.equals(flight.getDepartureDate()) ||
                            datepicker.before(flight.getDepartureDate())) && flight.getType().equals(type))
                        filteredFlights.add(flight);
                }
            }*/
            filteredFlights = flights.stream()
                    .filter(i -> (source.equals(i.getSource()) && destination.equals(i.getDestination())
                   && (i.getNoOfSeatsLeft()>noOfPassengers) && (datepicker.equals(i.getDepartureDate()) || datepicker.before(i.getDepartureDate()))
                   && type.equals(i.getType())))
                    .collect(Collectors.toList());
            if(type.equals("Economy")) {
                // flight = calculateFareForEconomy(filteredFlights,noOfPassengers);
                 flightsAfterFareCalculation = new EconomyFareCalculator();
            }
            else if(type.equals("Business")) {
                flightsAfterFareCalculation = new BusinessFareCalculator();
            }
            else {
                flightsAfterFareCalculation = new FirstFareCalculator();
            }
            flight = flightsAfterFareCalculation.calculateFare(filteredFlights,noOfPassengers,datepicker);
            return flight;
        }
    }

    /*private List<Flight> calculateFareForFirst(List<Flight> filteredFlights, int noOfPassengers,Date travelDate) {
        List<Flight> flights = new ArrayList<>();
        double fare;
        float diff;
        for(Flight flight:filteredFlights){
           // System.out.println( Math.abs((flight.getDepartureDate().getTime() - travelDate.getTime())/86400000));
            diff = Math.abs((flight.getDepartureDate().getTime() - travelDate.getTime())/86400000);

            fare =((flight.getFare() + (flight.getFare() * ((diff / 10))) )* noOfPassengers);
            flight.setFare(fare);
        }
        return filteredFlights;
    }*/

    /*private List<Flight> calculateFareForBusiness(List<Flight> filteredFlights, int noOfPassengers) {
        double fare =0;
        float percentage;
        String day;
        List<Flight> flights = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");

        for(Flight flight:filteredFlights){
            day = sdf.format(flight.getDepartureDate());
           // System.out.println(day);
            if(day.equals("Monday") || day.equals("Friday") || day.equals("Sunday"))
                fare = ((0.4 * flight.getFare()) + flight.getFare() ) * noOfPassengers;
            else
                fare = flight.getFare() * noOfPassengers;
                flight.setFare(fare);
                flights.add(flight);
        }
        return flights;
    }*/

/*    public List<Flight> calculateFareForEconomy(List<Flight> filteredFlights,int noOfPassengers){
        double fare=0;
        float percentage;
        List<Flight> flights = new ArrayList<>();
        for(Flight flight:filteredFlights) {
           // System.out.println(((float)(flight.getTotalNoOfSeats() - flight.getNoOfSeatsLeft() + noOfPassengers) / (flight.getTotalNoOfSeats()))*100);
            percentage = ((float)(flight.getTotalNoOfSeats() - flight.getNoOfSeatsLeft() + noOfPassengers) / (flight.getTotalNoOfSeats()))*100 ;
            if (percentage >= 0 && percentage < 40)
                fare = flight.getFare();
            else if (percentage >= 40 && percentage < 90)
                fare = flight.getFare() + (0.30 * flight.getFare());
            else if (percentage >= 90 && percentage <= 100)
                fare = flight.getFare() + (0.60 * flight.getFare());

            flight.setFare(fare*noOfPassengers);
            flights.add(flight);
        }
        return flights;

    }*/
}


