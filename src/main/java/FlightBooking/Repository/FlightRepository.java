package FlightBooking.Repository;

import FlightBooking.Model.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class FlightRepository {

    @Autowired
    public JdbcTemplate jdbcTemplate;

    //private static List<Flight> flightRepository = new ArrayList<>();
    SimpleDateFormat format1 = new SimpleDateFormat();

   public List<Flight> allFlights() throws ParseException
   {
         List<Flight> flightRepository = new ArrayList<>();
        flightRepository.add(new Flight(1,"Hyderabad","Latvia","Boeing777",
                0,10,100, format1.parse(format1.format(new Date())),"Business",6000));
        flightRepository.add(new Flight(2,"Hyderabad","Latvia","AirBusA321",
                0,100,100, format1.parse(format1.format(new Date())),"Economy",5000));
        flightRepository.add(new Flight(3,"Hyderabad","Latvia","AirBusA319",
                0,100,100, format1.parse(format1.format(new Date())),"First",4000));
       flightRepository.add(new Flight(1,"Hyderabad","Latvia","AirBusA321",
               0,10,100, format1.parse(format1.format(new Date())),"Business",5000));
        flightRepository.add(new Flight(4,"Latvia","Hyderabad","Boeing777",
                0,100,100, format1.parse(format1.format(new Date())),"Business",6000));
        flightRepository.add(new Flight(5,"Latvia","Hyderabad","AirBusA321",
                0,100,100, format1.parse(format1.format(new Date())),"First",5000));
        flightRepository.add(new Flight(6,"Latvia","Hyderabad","AirBusA319",
                0,100,100, format1.parse(format1.format(new Date())),"Business",4000));
        return flightRepository;
    }
    public  List<Flight> getAll(){
        String sql = "select * from flight;";
        List<Map<String, Object>> flights = jdbcTemplate.queryForList(sql);
        for(Map<String,Object> flight:flights) {
            System.out.println(flight.get("id"));
            System.out.println(flight.get("name"));
        }
        return new ArrayList<>();
    }
}
