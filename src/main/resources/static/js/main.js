$(document).ready(function () {

    $("#flight-search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        var search = {}
        search["source"] = $("#Source").val();

        search["destination"] = $("#Destination").val();

        search["travellers"] = document.getElementById('Travellers').value;

       // search["travellers"] = $("#Travellers").val();

        search["datepicker"] = $("#datepicker").val();

        search["flightType"] = document.getElementById('travelClass').value;

      //  var flightType;

      /*  if(document.getElementById('business').checked){
            flightType = document.getElementById('business').value;
        }
        else if(document.getElementById("economy").checked){
            flightType = document.getElementById('economy').value;
        }
        else if(document.getElementById("first").checked){
            flightType = document.getElementById("first").value;
        }
        search["flightType"] = flightType;
*/
        $("#btn-search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/api/search",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var json = "<h4>Flight Details</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
            console.log("data",data);
           var names =  data.result.map(i=>i.name);
           var departureDate = data.result.map(i=>i.departureDate);
           var fares = data.result.map(i=>i.fare);
            console.log(names);
            console.log(fares);
            console.log(departureDate);
            var array1 = new Array("", names,fares,departureDate);
           // displayTable(array1);
            //console.log("result",result);
          // $('#feedback').html(json);
            function formatDate(date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                return [year, month, day].join('-');
            }
            $(document).ready(function () {
                let html;
                if(names.length!=0) {
                  html= "<table class=\"table table-striped table-dark table-bordered table-hover table-sm table-responsive\">" + "<TH class=\"thead-dark\"> Number</TH><TH>Flight Name</TH><TH>Fare</TH><TH>Departure Date</TH>" + "</tr>";
                    for (var i = 0, j = 1; i < names.length; i++, j++) {
                        html += "<tr class=\"bg-success\" >";
                        html += "<td>" + j + "</td>";
                        html += "<td>" + names[i] + "</td>";
                        html += "<td>" + fares[i] + "</td>";
                        html += "<td>" + formatDate(departureDate[i]) + "</td>";
                        html += "</tr>";
                    }
                    html+="</table>";
                }
                else if(names.length===0)
                     html="<h1>No Flights Found</h1>";
                $('#feedback').html(html);
            });

            console.log("SUCCESS : ", data);
            $("#btn-search").prop("disabled", false);

        },
        error: function (e) {

            var json = "<h4>Flight Details </h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').html(json);

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);

        }
    });

    });

});