package FlightBooking.Service;

import FlightBooking.Model.Flight;
import FlightBooking.Repository.FlightRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceTest {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    @Mock
    FlightRepository flightRepository;

    @Test
    public void searchIfFlightsDoesntExistsInDatabase() throws ParseException {
        when(flightRepository.allFlights()).thenReturn(new ArrayList<>());
        SearchService searchService = new SearchService(flightRepository);
        List<Flight> flights = searchService.search("Hyderabad","Pune",0, format.parse(format.format(new Date())),"Business");
        assertEquals(0,flights.size());

    }

    @Test
    public void searchIfFlightsExistsAndPassengersIsZero() throws ParseException {
        ArrayList<Flight> flights = new ArrayList<>();
        flights.add(new Flight(1,"Hyderabad","Latvia","Boeing77",
                0,100,100, format.parse(format.format(new Date())),"Business",5000));
        flights.add(new Flight(2,"Hyderabad","Latvia","AirBusA321",
                0,100,100, format.parse(format.format(new Date())),"Business",5000));
        flights.add(new Flight(3,"Hyderabad","Latvia","AirBusA319",
                0,100,100, format.parse(format.format(new Date())),"Business",5000));

        when(flightRepository.allFlights()).thenReturn(flights);
        SearchService searchService = new SearchService(flightRepository);
        List<Flight> flight = searchService.search("Hyderabad","Latvia",0, format.parse(format.format(new Date())),"Business");
        assertEquals(3,flight.size());

    }
    @Test
    public void searchWhenFlightsExistsButNotTheRequestedOneAndPassengersIsZero() throws ParseException {
        ArrayList<Flight> flights = new ArrayList<>();
        flights.add(new Flight(1, "Hyderabad", "Latvia", "Boeing77",
                0,100,100, format.parse(format.format(new Date())),"Business"));
        flights.add(new Flight(2, "Hyderabad", "Latvia", "AirBusA321",
                0,100,100, format.parse(format.format(new Date())),"Business"));
        flights.add(new Flight(3, "Hyderabad", "Latvia", "AirBusA319",
                0,100,100, format.parse(format.format(new Date())),"Business"));

        when(flightRepository.allFlights()).thenReturn(flights);
        SearchService searchService = new SearchService(flightRepository);
        List<Flight> flight = searchService.search("Hyderabad","Goa",0, format.parse(format.format(new Date())),"Business");
        assertEquals(0,flight.size());
    }
    @Test
    public void searchIfFlightsExistsAndPassengersIsPassed() throws ParseException {
        ArrayList<Flight> flights = new ArrayList<>();
        flights.add(new Flight(1,"Hyderabad","Latvia","Boeing77",
                0,100,100, format.parse(format.format(new Date())),"Business",5000));
        flights.add(new Flight(2,"Hyderabad","Latvia","AirBusA321",
                0,100,100, format.parse(format.format(new Date())),"Business",5000));
        flights.add(new Flight(3,"Hyderabad","Latvia","AirBusA319",
                0,100,100, format.parse(format.format(new Date())),"Business",5000));

        when(flightRepository.allFlights()).thenReturn(flights);
        SearchService searchService = new SearchService(flightRepository);
        List<Flight> flight = searchService.search("Hyderabad","Latvia",2,
                format.parse(format.format(new Date())),"Business");
        assertEquals(3,flight.size());
    }
    @Test
    public void searchWhenFlightsExistsButNotTheRequestedOneAndPassengersIsPassed() throws ParseException {
        ArrayList<Flight> flights = new ArrayList<>();
        flights.add(new Flight(1, "Hyderabad", "Latvia", "Boeing77",
                0,100,100, format.parse(format.format(new Date())),"Business"));
        flights.add(new Flight(2, "Hyderabad", "Latvia", "AirBusA321",
                0,100,100, format.parse(format.format(new Date())),"Business"));
        flights.add(new Flight(3, "Hyderabad", "Latvia", "AirBusA319",
                0,100,100, format.parse(format.format(new Date())),"Business"));

        when(flightRepository.allFlights()).thenReturn(flights);
        SearchService searchService = new SearchService(flightRepository);
        List<Flight> flight = searchService.search("Hyderabad","Goa",1, format.parse(format.format(new Date())),"Business");
        assertEquals(0,flight.size());
    }
    @Test
    public void searchWhenFlightsExistsAndPassengersIsPassedButGreaterThanTheAirwayLimit() throws ParseException {
        ArrayList<Flight> flights = new ArrayList<>();
        flights.add(new Flight(1, "Hyderabad", "Latvia", "Boeing77",
                0,100,100, format.parse(format.format(new Date())),"Business"));
        flights.add(new Flight(2, "Hyderabad", "Latvia", "AirBusA321",
                0,100,100, format.parse(format.format(new Date())),"Business"));
        flights.add(new Flight(3, "Hyderabad", "Latvia", "AirBusA319",
                0,100,100, format.parse(format.format(new Date())),"Business"));

        when(flightRepository.allFlights()).thenReturn(flights);
        SearchService searchService = new SearchService(flightRepository);
        List<Flight> flight = searchService.search("Hyderabad","Latvia",101, format.parse(format.format(new Date())),"Business");
        assertEquals(0,flight.size());
    }
    @Test
    public void searchWhenDepartureDateIsLessThanTheRequestedDate() throws ParseException{
        ArrayList<Flight> flights = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat();
        String newDate;
        Date date;
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(sdf.format(new Date())));
        //Incrementing the date by 1 day
        c.add(Calendar.DAY_OF_MONTH, -1);
        newDate= sdf.format(c.getTime());
        date = sdf.parse(newDate);

        flights.add(new Flight(1, "Hyderabad", "Latvia", "Boeing77",
                0,100,100, date,"Business"));
        flights.add(new Flight(2, "Hyderabad", "Latvia", "AirBusA321",
                0,100,100, date,"Business"));
        flights.add(new Flight(3, "Hyderabad", "Latvia", "AirBusA319",
                0,100,100, date,"Business"));

        when(flightRepository.allFlights()).thenReturn(flights);
        SearchService searchService = new SearchService(flightRepository);
        List<Flight> flight = searchService.search("Hyderabad","Latvia",101, format.parse(format.format(new Date())),"Business");
        assertEquals(0,flight.size());

    }
    @Test
    public void searchBasedOnNoOfSeatsLeftToCalculateTheFareAmount() throws ParseException{
        String newDate;
        Date date;
        ArrayList<Flight> flights = new ArrayList<>();
        SimpleDateFormat format1 = new SimpleDateFormat();
        Calendar c = Calendar.getInstance();
        try{
            c.setTime(format1.parse(format1.format(new Date())));
        }catch(ParseException e){
            e.printStackTrace();
        }
        //Incrementing the date by 1 day
        c.add(Calendar.DAY_OF_MONTH, 9);
        newDate= format1.format(c.getTime());
        date = format1.parse(newDate);


        System.out.println(format1.parse(format1.format(new Date())));
        flights.add(new Flight(1,"Hyderabad","Latvia","Boeing777",
                0,62,100, date,"Business",6000));
        flights.add(new Flight(2,"Hyderabad","Latvia","AirBusA321",
                0,62,100,date,"Economy",5000));
        flights.add(new Flight(3,"Hyderabad","Latvia","AirBusA319",
                0,100,100, date,"First",4000));
        when(flightRepository.allFlights()).thenReturn(flights);
        SearchService searchService = new SearchService(flightRepository);
        List<Flight> economyFlights = searchService.search("Hyderabad","Latvia",4,
                format.parse(format.format(new Date())),"Economy");
       // assertEquals(19200,);
        List<Flight> firstFlights = searchService.search("Hyderabad","Latvia",4,
                format.parse(format.format(new Date())),"First");
        List<Flight> businessFlight = searchService.search("Hyderabad","Latvia",4,
                format.parse(format.format(new Date())),"Business");
        economyFlights.forEach(flight1 -> assertEquals(26000, flight1.getFare(),0));
        firstFlights.forEach(flight1 -> assertEquals(17600.000023841858,flight1.getFare(),0));
        businessFlight.forEach(flight1 -> assertEquals(33600.0,flight1.getFare(),0));

    }



}