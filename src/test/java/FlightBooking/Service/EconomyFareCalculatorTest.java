package FlightBooking.Service;

import FlightBooking.Model.Flight;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)

public class EconomyFareCalculatorTest {
    @Mock
    SearchService searchService;
    FareCalculator fareCalculator = new EconomyFareCalculator();


    SimpleDateFormat format1 = new SimpleDateFormat();
    @Test
    public void calculateEconomyFare() throws ParseException {
        List<Flight> economyFlights = new ArrayList<>();
        List<Flight> flights = new ArrayList<>();

        economyFlights.add(new Flight(1,"Hyderabad","Latvia","Boeing777",
                0,10,100, format1.parse(format1.format(new Date())),"Economy",5000));
        flights = fareCalculator.calculateFare(economyFlights,2,format1.parse(format1.format(new Date())));
        //economyFlights.forEach(flight1 -> assertEquals(26000, flight1.getFare(),0));
        flights.forEach(flight1 -> assertEquals(16000.0,flight1.getFare(),0));
        //businessFlight.forEach(flight1 -> assertEquals(33600.0,flight1.getFare(),0));
        //  Assert.assertEquals(17600.000023841858,economyFlights.);

    }


}

